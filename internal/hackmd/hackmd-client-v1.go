package hackmd

import (
	"encoding/json"
	"fmt"
	"mdmigrate/internal/config"
	"mdmigrate/internal/domain"
	"mdmigrate/pkg/logger"
	"net/http"
	"net/url"
)

type hackmdClientV1 struct {
	cfg *config.HackMDConfig
	log logger.ILogger
}

func ClientV1(cfg *config.HackMDConfig, log logger.ILogger) IHackmdClient {
	return &hackmdClientV1{
		cfg: cfg,
		log: log,
	}
}
func (c *hackmdClientV1) withBearerToken(req *http.Request) {
	req.Header.Add("Authorization", "Bearer "+c.cfg.APIToken)
}
func convertToINotes(notes []note) []domain.INote {
	result := make([]domain.INote, len(notes))
	for i := range notes {
		result[i] = &notes[i]
	}
	return result
}

func (c *hackmdClientV1) GetAllNotes() ([]domain.INote, error) {
	c.log.Message("Request: GetAllNotes")

	requestURL, err := url.JoinPath(c.cfg.BaseUrl, "v1", "notes")
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest(http.MethodGet, requestURL, nil)
	if err != nil {
		return nil, err
	}

	c.withBearerToken(req)

	response, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	result := []note{}
	err = json.NewDecoder(response.Body).Decode(&result)
	if err != nil {
		return nil, err
	}

	return convertToINotes(result), nil
}
func (c *hackmdClientV1) GetNoteByID(noteID string) (domain.INote, error) {
	c.log.Message(fmt.Sprintf("Request: GetNoteByID('%s')", noteID))
	requestURL, err := url.JoinPath(c.cfg.BaseUrl, "v1", "notes", noteID)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest(http.MethodGet, requestURL, nil)
	if err != nil {
		return nil, err
	}

	c.withBearerToken(req)

	response, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	result := &note{}
	err = json.NewDecoder(response.Body).Decode(result)
	if err != nil {
		return nil, err
	}

	return result, nil
}

type note struct {
	ID      string `json:"id"`
	Title   string `json:"title"`
	Content string `json:"content"`
}

func (n *note) GetID() string {
	return n.ID
}
func (n *note) GetTitle() string {
	return n.Title
}
func (n *note) GetContent() string {
	return n.Content
}
