package hackmd

import (
	"mdmigrate/internal/domain"

	"github.com/stretchr/testify/mock"
)

type MockHackmdClient struct {
	mock.Mock
}

func (m *MockHackmdClient) GetAllNotes() ([]domain.INote, error) {
	args := m.Called()
	notes := args.Get(0)
	if notes != nil {
		return args.Get(0).([]domain.INote), args.Error(1)
	}
	return nil, args.Error(1)
}
func (m *MockHackmdClient) GetNoteByID(noteID string) (domain.INote, error) {
	args := m.Called(noteID)
	note := args.Get(0)
	if note != nil {
		return args.Get(0).(domain.INote), args.Error(1)
	}
	return nil, args.Error(1)
}
