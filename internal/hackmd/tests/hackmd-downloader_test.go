package hackmd

import (
	"errors"
	"io/fs"
	"mdmigrate/internal/config"
	"mdmigrate/internal/domain"
	"mdmigrate/internal/hackmd"
	"mdmigrate/pkg/logger"
	"mdmigrate/pkg/osp"
	"testing"

	domaintest "mdmigrate/internal/domain/tests"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

var defaultTestCfg = &config.HackMDConfig{TmpFolder: "./hackmd", TopicNameMaxLen: 10}

func TestCreateTempFolder_StatError(t *testing.T) {
	cfg := defaultTestCfg

	osp := &osp.MockOsProvider{}
	osp.On("Stat", cfg.TmpFolder).Return(nil, fs.ErrInvalid)

	client := &MockHackmdClient{}

	d := hackmd.CreateDownloader(cfg, &logger.DefaultLogger{}, osp, client, &MockMediaDownloader{})
	err := d.Run()

	osp.AssertExpectations(t)
	client.AssertExpectations(t)

	assert.EqualError(t, err, fs.ErrInvalid.Error())
}
func TestCreateTempFolder_MkdirError(t *testing.T) {
	cfg := defaultTestCfg

	osp := &osp.MockOsProvider{}
	osp.On("Stat", cfg.TmpFolder).Return(nil, fs.ErrNotExist)
	osp.On("Mkdir", cfg.TmpFolder, mock.Anything).Return(fs.ErrPermission)

	client := &MockHackmdClient{}

	d := hackmd.CreateDownloader(cfg, &logger.DefaultLogger{}, osp, client, &MockMediaDownloader{})
	err := d.Run()

	osp.AssertExpectations(t)
	client.AssertExpectations(t)
	assert.EqualError(t, err, fs.ErrPermission.Error())
}
func TestCreateTempFolder_Success(t *testing.T) {
	cfg := defaultTestCfg

	osp := &osp.MockOsProvider{}
	osp.On("Stat", cfg.TmpFolder).Return(nil, fs.ErrNotExist)
	osp.On("Mkdir", cfg.TmpFolder, mock.Anything).Return(nil)

	client := &MockHackmdClient{}
	client.On("GetAllNotes").Return(nil, nil)

	d := hackmd.CreateDownloader(cfg, &logger.DefaultLogger{}, osp, client, &MockMediaDownloader{})
	err := d.Run()

	osp.AssertExpectations(t)
	client.AssertExpectations(t)
	assert.NoError(t, err)
}
func TestDownloadNotes_Client_GetAllNotes_Error(t *testing.T) {
	cfg := defaultTestCfg

	osp := &osp.MockOsProvider{}
	osp.On("Stat", cfg.TmpFolder).Return(nil, fs.ErrNotExist)
	osp.On("Mkdir", cfg.TmpFolder, mock.Anything).Return(nil)

	client := &MockHackmdClient{}
	clientTestError := errors.New("client error")
	client.On("GetAllNotes").Return(nil, clientTestError)

	d := hackmd.CreateDownloader(cfg, &logger.DefaultLogger{}, osp, client, &MockMediaDownloader{})
	err := d.Run()

	osp.AssertExpectations(t)
	client.AssertExpectations(t)
	assert.EqualError(t, err, clientTestError.Error())
}
func TestDownloadNotes_Client_GetNoteByID_Error(t *testing.T) {
	cfg := defaultTestCfg

	osp := &osp.MockOsProvider{}
	osp.On("Stat", cfg.TmpFolder).Return(nil, nil).
		On("Stat", mock.Anything).Return(nil, fs.ErrNotExist)

	note := domaintest.CreateMockNote("testID", "test title", "test content")
	clientTestError := errors.New("client error")

	client := &MockHackmdClient{}
	client.On("GetAllNotes").Return([]domain.INote{note}, nil)
	client.On("GetNoteByID", note.GetID()).Return(nil, clientTestError)

	d := hackmd.CreateDownloader(cfg, &logger.DefaultLogger{}, osp, client, &MockMediaDownloader{})
	err := d.Run()

	osp.AssertExpectations(t)
	client.AssertExpectations(t)
	assert.EqualError(t, err, clientTestError.Error())
}
func TestDownloadNotes_SaveNote_NoteFolderError(t *testing.T) {
	cfg := defaultTestCfg

	osp := &osp.MockOsProvider{}
	client := &MockHackmdClient{}
	d := hackmd.CreateDownloader(cfg, &logger.DefaultLogger{}, osp, client, &MockMediaDownloader{})

	note := domaintest.CreateMockNote("testID", "test title", "test content")

	osp.On("Stat", cfg.TmpFolder).Return(nil, nil).
		On("Stat", domain.GetNoteFolderPath(note, cfg)).Return(nil, fs.ErrPermission)

	client.On("GetAllNotes").Return([]domain.INote{note}, nil)

	err := d.Run()

	osp.AssertExpectations(t)
	client.AssertExpectations(t)
	assert.EqualError(t, err, fs.ErrPermission.Error())
}
func TestDownloadNotes_SaveNote_NoteFileError(t *testing.T) {
	cfg := defaultTestCfg

	osp := &osp.MockOsProvider{}
	client := &MockHackmdClient{}
	d := hackmd.CreateDownloader(cfg, &logger.DefaultLogger{}, osp, client, &MockMediaDownloader{})

	note := domaintest.CreateMockNote("testID", "test title", "test content")

	osp.On("Stat", cfg.TmpFolder).Return(nil, nil).
		On("Stat", domain.GetNoteFolderPath(note, cfg)).Return(nil, fs.ErrNotExist).
		On("Mkdir", domain.GetNoteFolderPath(note, cfg), mock.Anything).Return(nil).
		On("WriteFile", mock.Anything, mock.Anything, mock.Anything).Return(fs.ErrPermission)

	client.On("GetAllNotes").Return([]domain.INote{note}, nil)
	client.On("GetNoteByID", note.GetID()).Return(note, nil)

	err := d.Run()

	osp.AssertExpectations(t)
	client.AssertExpectations(t)
	assert.EqualError(t, err, fs.ErrPermission.Error())
}
func TestDownloadNotes_SaveNote_Success(t *testing.T) {
	cfg := defaultTestCfg

	osp := &osp.MockOsProvider{}
	client := &MockHackmdClient{}
	d := hackmd.CreateDownloader(cfg, &logger.DefaultLogger{}, osp, client, &MockMediaDownloader{})

	note := domaintest.CreateMockNote("testID", "test title", "test content")
	osp.On("Stat", cfg.TmpFolder).Return(nil, nil).
		On("Stat", domain.GetNoteFolderPath(note, cfg)).Return(nil, fs.ErrNotExist).
		On("Mkdir", domain.GetNoteFolderPath(note, cfg), mock.Anything).Return(nil).
		On("WriteFile", mock.Anything, mock.Anything, mock.Anything).Return(nil)

	client.On("GetAllNotes").Return([]domain.INote{note}, nil)
	client.On("GetNoteByID", note.GetID()).Return(note, nil)

	osp.On("WriteFile", domain.GetNoteFilePath(note, cfg), mock.Anything, mock.Anything).Return(nil)

	err := d.Run()

	osp.AssertExpectations(t)
	client.AssertExpectations(t)
	assert.NoError(t, err)
}
func TestDownloadNotes_SaveNote_NoteTempFileExists(t *testing.T) {
	cfg := defaultTestCfg

	osp := &osp.MockOsProvider{}
	osp.On("Stat", cfg.TmpFolder).Return(nil, nil).
		On("Stat", mock.Anything).Return(nil, nil) // note temp file exists

	note := domaintest.CreateMockNote("testID", "test title", "test content")

	client := &MockHackmdClient{}
	client.On("GetAllNotes").Return([]domain.INote{note}, nil)

	d := hackmd.CreateDownloader(cfg, &logger.DefaultLogger{}, osp, client, &MockMediaDownloader{})
	err := d.Run()

	osp.AssertExpectations(t)
	client.AssertExpectations(t)
	assert.NoError(t, err, fs.ErrPermission.Error())
}
func TestDownloadNotes_SaveNote_FirstNoteExists_SecondDoesntExist(t *testing.T) {
	cfg := defaultTestCfg

	osp := &osp.MockOsProvider{}
	osp.On("Stat", cfg.TmpFolder).Return(nil, nil)

	client := &MockHackmdClient{}

	note1 := domaintest.CreateMockNote("testID1", "title1", "test content")
	note2 := domaintest.CreateMockNote("testID2", "title2", "test content")
	osp.On("Stat", domain.GetNoteFolderPath(note1, cfg)).Return(nil, nil).
		On("Stat", domain.GetNoteFolderPath(note2, cfg)).Return(nil, fs.ErrNotExist).
		On("Mkdir", domain.GetNoteFolderPath(note2, cfg), mock.Anything).Return(nil).
		On("WriteFile", domain.GetNoteFilePath(note2, cfg), mock.Anything, mock.Anything).Return(nil)

	client.On("GetAllNotes").Return([]domain.INote{note1, note2}, nil).
		On("GetNoteByID", note2.GetID()).Return(note2, nil)

	d := hackmd.CreateDownloader(cfg, &logger.DefaultLogger{}, osp, client, &MockMediaDownloader{})
	err := d.Run()

	osp.AssertExpectations(t)
	client.AssertExpectations(t)
	assert.NoError(t, err, fs.ErrPermission.Error())
}
func TestDownloadImages_Save(t *testing.T) {
	cfg := &config.HackMDConfig{TmpFolder: "/test", TopicNameMaxLen: 10}

	testContentWithLink := `![](https://hackmd.io/_uploads/1.png)`
	note := domaintest.CreateMockNote("testID1", "title1", testContentWithLink)
	links := domain.GetMediaLinks(testContentWithLink)

	osp := &osp.MockOsProvider{}
	client := &MockHackmdClient{}
	md := &MockMediaDownloader{}

	d := hackmd.CreateDownloader(cfg, &logger.DefaultLogger{}, osp, client, md)
	osp.On("Stat", cfg.TmpFolder).Return(nil, nil).
		On("Stat", domain.GetNoteFolderPath(note, cfg)).Return(nil, fs.ErrNotExist).
		On("Mkdir", domain.GetNoteFolderPath(note, cfg), mock.Anything).Return(nil).
		On("WriteFile", domain.GetNoteFilePath(note, cfg), mock.Anything, mock.Anything).Return(nil)

	client.On("GetAllNotes").Return([]domain.INote{note}, nil).
		On("GetNoteByID", note.GetID()).Return(note, nil)

	md.On("DownloadMedia", domain.GetNoteFolderPath(note, cfg), links[0]).Return(nil)

	err := d.Run()
	assert.NoError(t, err)

	client.AssertExpectations(t)
	md.AssertExpectations(t)
	osp.AssertExpectations(t)
}
