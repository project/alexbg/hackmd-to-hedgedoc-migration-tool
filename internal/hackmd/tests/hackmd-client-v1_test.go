package hackmd

import (
	"mdmigrate/internal/config"
	"mdmigrate/internal/hackmd"
	"mdmigrate/pkg/logger"
	"mdmigrate/pkg/osp"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetAllNotes(t *testing.T) {
	cfg := config.Config{Logger: &logger.DefaultLogger{}}
	err := cfg.Load("../../../.secrets/config-test.yaml", &osp.DefaultOsProvider{})
	assert.NoError(t, err)

	c := hackmd.ClientV1(&cfg.HackMD, cfg.Logger)
	notes, err := c.GetAllNotes()
	assert.NoError(t, err)
	assert.NotZero(t, len(notes))
}
func TestGetNoteByID(t *testing.T) {
	cfg := config.Config{Logger: &logger.DefaultLogger{}}
	err := cfg.Load("../../../.secrets/config-test.yaml", &osp.DefaultOsProvider{})
	assert.NoError(t, err)

	testNoteID := "9cIwhgXUQv-1sjy0G_UG5A"
	c := hackmd.ClientV1(&cfg.HackMD, cfg.Logger)
	note, err := c.GetNoteByID(testNoteID)
	assert.NoError(t, err)
	assert.Equal(t, testNoteID, note.GetID())
	assert.Equal(t, "# Test Note\n\ntest content", note.GetContent())
}
