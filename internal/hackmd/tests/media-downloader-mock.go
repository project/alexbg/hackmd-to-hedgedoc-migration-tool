package hackmd

import (
	"mdmigrate/internal/domain"

	"github.com/stretchr/testify/mock"
)

type MockMediaDownloader struct {
	mock.Mock
}

func (m *MockMediaDownloader) DownloadMedia(folder string, link *domain.LinkMarkup) error {
	args := m.Called(folder, link)
	return args.Error(0)
}
