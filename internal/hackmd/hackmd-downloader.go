package hackmd

import (
	"errors"
	"io/fs"
	"mdmigrate/internal/config"
	"mdmigrate/internal/domain"
	"mdmigrate/pkg/logger"
	"mdmigrate/pkg/osp"
	"path"
)

type IDownloader interface {
	Run() error
}

type downloader struct {
	cfg             *config.HackMDConfig
	log             logger.ILogger
	osp             osp.IOsProvider
	client          IHackmdClient
	mediaDownloader IMediaDownloader
}

func CreateDownloader(cfg *config.HackMDConfig, log logger.ILogger, osp osp.IOsProvider,
	client IHackmdClient, md IMediaDownloader) IDownloader {
	return &downloader{
		cfg:             cfg,
		log:             log,
		osp:             osp,
		client:          client,
		mediaDownloader: md,
	}
}

func (d *downloader) createFolderIfNotExist(folderPath string) error {
	osp := d.osp

	_, err := osp.Stat(folderPath)
	if err == nil {
		d.log.Message("HACKMD: Choosing folder : %s", folderPath)
		return nil
	} else if !errors.Is(err, fs.ErrNotExist) {
		return err
	}

	d.log.Message("HACKMD: Temp folder isn't exists: %s", folderPath)
	if err = osp.Mkdir(folderPath, fs.ModePerm); err != nil {
		return err
	}

	d.log.Message("HACKMD: Temp folder created: %s", folderPath)
	return nil
}

func (d *downloader) Run() error {
	err := d.createFolderIfNotExist(d.cfg.TmpFolder)
	if err != nil {
		return err
	}

	notes, err := d.client.GetAllNotes()
	if err != nil {
		return err
	}

	for _, note := range notes {
		tmpNotePath, noteFileName := domain.GetNoteTmpFolderAndFilePath(note, d.cfg)
		_, err := d.osp.Stat(tmpNotePath)
		if err == nil {
			d.log.Message("HACKMD: Temp file already exists. Downloading Skiped: %s", tmpNotePath)
			continue
		} else if !errors.Is(err, fs.ErrNotExist) {
			return err
		}

		fullNote, err := d.client.GetNoteByID(note.GetID())
		if err != nil {
			return err
		}

		err = d.downloadWithImages(fullNote, tmpNotePath, noteFileName)
		if err != nil {
			return err
		}
	}
	return nil
}

func (d *downloader) downloadWithImages(fullNote domain.INote, tmpNotePath string, noteFileName string) error {
	err := d.createFolderIfNotExist(tmpNotePath)
	if err != nil {
		return err
	}

	filePath := path.Join(tmpNotePath, noteFileName)

	content := fullNote.GetContent()
	if err := d.osp.WriteFile(filePath, []byte(content), 0666); err != nil {
		return err
	}

	d.log.Message("HACKMD: Note Saved: %s", filePath)

	links := domain.GetMediaLinks(content)
	for i := 0; i < len(links); i++ {
		if err = d.mediaDownloader.DownloadMedia(tmpNotePath, links[i]); err != nil {
			return err
		}
	}

	return nil
}
