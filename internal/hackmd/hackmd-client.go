package hackmd

import "mdmigrate/internal/domain"

type IHackmdClient interface {
	GetAllNotes() ([]domain.INote, error)
	GetNoteByID(noteID string) (domain.INote, error)
}
