package hackmd

import (
	"io"
	"mdmigrate/internal/domain"
	"mdmigrate/pkg/logger"
	"mdmigrate/pkg/osp"
	"net/http"
	"path"
	"path/filepath"
	"strings"

	"golang.org/x/exp/slices"
)

type IMediaDownloader interface {
	DownloadMedia(folder string, link *domain.LinkMarkup) error
}

type MediaDownloader struct {
	uploadsPath string
	osp         osp.IOsProvider
	logger      logger.ILogger
}

func CreateMediaDownloader(osp osp.IOsProvider, log logger.ILogger) *MediaDownloader {
	result := &MediaDownloader{
		osp:    osp,
		logger: log,
	}
	return result
}

var validExt = []string{".png", ".bmp", ".jpg", ".gif"}

func (d *MediaDownloader) DownloadMedia(folder string, link *domain.LinkMarkup) error {
	ext := strings.ToLower(filepath.Ext(link.Source))
	if !slices.Contains(validExt, ext) {
		return nil
	}

	d.logger.Message("Media: Downloading %s", link.Source)
	filePath := path.Join(folder, link.GetSourceFileName())

	req, err := http.NewRequest(http.MethodGet, link.Source, nil)
	if err != nil {
		return err
	}
	req.Header.Add("User-Agent", "PostmanRuntime/7.33.0") // fix for error 429 "To Many Requests" from imgur.com

	response, err := http.DefaultClient.Do(req)
	if err != nil {
		d.logger.Warning("Media: WARNING %s", err.Error())
		return nil
	}
	defer response.Body.Close()

	if response.ContentLength <= 0 {
		d.logger.Warning("Media: WARNING: ContentLength = 0")
		return nil
	}

	d.logger.Message("Media: Saving %s", filePath)
	out, err := d.osp.Create(filePath)
	if err != nil {
		return err

	}
	defer out.Close()
	bodyBytes, err := io.ReadAll(response.Body)
	if err != nil {
		return err
	}

	if err := d.osp.WriteFile(filePath, bodyBytes, 0666); err != nil {
		return err
	}

	d.logger.Message("Media: Saved %v bytes", len(bodyBytes))
	return nil
}
