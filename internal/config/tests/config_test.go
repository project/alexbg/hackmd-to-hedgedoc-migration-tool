package config

import (
	"errors"
	"io/ioutil"
	"testing"

	"mdmigrate/internal/config"
	"mdmigrate/pkg/osp"

	"github.com/stretchr/testify/assert"
)

func getTestData(t *testing.T, filePath string) string {
	bytes, err := ioutil.ReadFile(filePath)
	assert.NoError(t, err)
	return string(bytes)
}

func TestConfig_Load_NoFile(t *testing.T) {
	testConfigPath := "/test/config.yaml"

	var osp osp.MockOsProvider
	osp.On("Stat", testConfigPath).Return(nil, errors.New("no file"))

	cfg := &config.Config{}
	err := cfg.Load(testConfigPath, &osp)
	assert.Error(t, err)
}
func TestConfig_Load_ReadFileError(t *testing.T) {
	testConfigPath := "/test/config.yaml"

	var osp osp.MockOsProvider
	osp.On("Stat", testConfigPath).Return(nil, nil).
		On("ReadFile", testConfigPath).Return(nil, errors.New("file read error"))

	cfg := &config.Config{}
	err := cfg.Load(testConfigPath, &osp)
	assert.Error(t, err)
}

func TestConfig_Load(t *testing.T) {
	testConfigPath := "/test/config.yaml"

	var osp osp.MockOsProvider
	osp.On("Stat", testConfigPath).Return(nil, nil).
		On("ReadFile", testConfigPath).Return(getTestData(t, "test_config.yaml"), nil)

	cfg := &config.Config{}
	err := cfg.Load(testConfigPath, &osp)

	assert.NoError(t, err)
	assert.Equal(t, "hackmd.test/v1", cfg.HackMD.BaseUrl)
	assert.Equal(t, "HACKMD_TEST_API_TOKEN", cfg.HackMD.APIToken)
	assert.Equal(t, "./hackmd", cfg.HackMD.TmpFolder)
	assert.Equal(t, 20, cfg.HackMD.TopicNameMaxLen)

	assert.Equal(t, "test_user", cfg.HedgeDoc.UserName)
	assert.Equal(t, "hedgedoc.test/", cfg.HedgeDoc.BaseUrl)
	assert.Equal(t, "test cookie", cfg.HedgeDoc.Cookie)
	assert.Equal(t, "http://test/uploads", cfg.HedgeDoc.UploadsUrl)
	assert.Equal(t, "/test/uploads", cfg.HedgeDoc.UploadsPath)
}
