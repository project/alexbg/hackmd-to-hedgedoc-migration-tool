package config

import (
	"mdmigrate/pkg/logger"
	"mdmigrate/pkg/osp"

	yaml "gopkg.in/yaml.v2"
)

type Config struct {
	HackMD   HackMDConfig   `yaml:"hackmd"`
	HedgeDoc HedgeDocConfig `yaml:"hedgedoc"`
	Logger   logger.ILogger
}

type HackMDConfig struct {
	BaseUrl         string `yaml:"baseUrl"`
	APIToken        string `yaml:"apiToken"`
	TmpFolder       string `yaml:"tmpFolder"`
	TopicNameMaxLen int    `yaml:"topicNameMaxLen"`
}

type HedgeDocConfig struct {
	UserName    string `yaml:"userName"`
	BaseUrl     string `yaml:"baseUrl"`
	Cookie      string `yaml:"cookie"`
	UploadsPath string `yaml:"uploads"`
	UploadsUrl  string `yaml:"uploadsUrl"`
}

func (cfg *Config) Load(configPath string, osp osp.IOsProvider) error {
	_, err := osp.Stat(configPath)
	if err != nil {
		return err
	}

	fileData, err := osp.ReadFile(configPath)
	if err != nil {
		return err
	}

	err = yaml.Unmarshal(fileData, cfg)
	if err != nil {
		return err
	}

	return nil
}
