package app

import (
	"fmt"
	"mdmigrate/internal/config"
	"mdmigrate/internal/hackmd"
	"mdmigrate/internal/hedgedoc"
	"mdmigrate/pkg/flags"
	"mdmigrate/pkg/logger"
	"mdmigrate/pkg/osp"
)

func Run() {
	flags := flags.DefaultFlagsProvider{}
	osp := &osp.DefaultOsProvider{}
	log := &logger.DefaultLogger{}

	cfg := &config.Config{Logger: log}

	var configPath string
	flags.StringVar(&configPath, "config", "config.yaml", "")
	flags.Parse()

	if err := cfg.Load(configPath, osp); err != nil {
		log.Fatal(fmt.Sprintf("Failed to Load configuration from '%s': %s", configPath, err.Error()))
	}

	hackMDClient := hackmd.ClientV1(&cfg.HackMD, log)
	mediaDownloader := hackmd.CreateMediaDownloader(osp, log)
	downloader := hackmd.CreateDownloader(&cfg.HackMD, log, osp, hackMDClient, mediaDownloader)
	if err := downloader.Run(); err != nil {
		log.Fatal(fmt.Sprintf("HackMD Downloader Error: %s", err.Error()))
	}

	hedgedocClient := hedgedoc.ClientV1(&cfg.HedgeDoc, log)
	uploader := hedgedoc.CreateUploader(cfg, hedgedocClient, osp)
	if err := uploader.Run(); err != nil {
		log.Fatal(fmt.Sprintf("Hedgedoc Uploader Error: %s", err.Error()))
	}
}
