package domain

import (
	"mdmigrate/internal/config"
	"mdmigrate/internal/domain"
	"testing"

	"github.com/stretchr/testify/assert"
)

var testNoteConfig = &config.HackMDConfig{TmpFolder: "", TopicNameMaxLen: 10}

func TestNotes_TempFileName_MaxLen1(t *testing.T) {
	note := CreateMockNote("testID", "test title", "")
	path := domain.GetNoteFolderPath(note, testNoteConfig)
	assert.Equal(t, "testID - test title.md", path)
}
func TestNotes_TempFileName_MaxLen2(t *testing.T) {
	note := CreateMockNote("testID", "title", "")
	path := domain.GetNoteFolderPath(note, testNoteConfig)
	assert.Equal(t, "testID - title.md", path)
}
func TestNotes_TempFileName_MaxLen_TrimSpaces(t *testing.T) {
	note := CreateMockNote("testID", "very very    long title", "")
	path := domain.GetNoteFolderPath(note, testNoteConfig)
	assert.Equal(t, "testID - very very.md", path)
}
func TestNotes_TempFileName_MaxLen3(t *testing.T) {
	cfg := &config.HackMDConfig{TmpFolder: "", TopicNameMaxLen: 8}
	note := CreateMockNote("testID", "test title", "")
	path := domain.GetNoteFolderPath(note, cfg)
	assert.Equal(t, "testID - test tit.md", path)
}
func TestNotes_TempFileName_MaxLen_Unicode(t *testing.T) {
	note := CreateMockNote("testID", "Поступление в ПТУ\\Техникум", "")
	path := domain.GetNoteFolderPath(note, testNoteConfig)
	assert.Equal(t, "testID - Поступлени.md", path)
}
func TestNotes_TempFileName_SpecialChars(t *testing.T) {
	note := CreateMockNote("ID%-*?", "TITLE%-*?", "")
	path := domain.GetNoteFolderPath(note, testNoteConfig)
	assert.Equal(t, "ID - TITLE.md", path)
}
