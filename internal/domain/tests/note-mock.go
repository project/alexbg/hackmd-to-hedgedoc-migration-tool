package domain

import "github.com/stretchr/testify/mock"

type MockNote struct {
	mock.Mock
}

func (m *MockNote) GetID() string {
	args := m.Called()
	return args.String(0)
}
func (m *MockNote) GetTitle() string {
	args := m.Called()
	return args.String(0)
}
func (m *MockNote) GetContent() string {
	args := m.Called()
	return args.String(0)
}

func CreateMockNote(id, title, content string) *MockNote {
	note := &MockNote{}
	note.On("GetID").Return(id).
		On("GetTitle").Return(title).
		On("GetContent").Return(content)
	return note
}
