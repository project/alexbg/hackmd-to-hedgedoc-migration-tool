package domain

import (
	"mdmigrate/internal/domain"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLinkMarkup_Parse(t *testing.T) {
	content :=
		`# Test Header
		Image Here: ![](https://hackmd.io/_uploads/111.png)
		Another Image with optional part: ![](https://hackmd.io/_uploads/222.png "optional part") 
		Another Image : ![image text](https://hackmd.io/_uploads/333.png "optional part") 
		`
	links := domain.GetMediaLinks(content)
	assert.NotNil(t, links)
	assert.Len(t, links, 3)

	// [0]
	link := links[0]
	assert.Equal(t, "![](https://hackmd.io/_uploads/111.png)", link.LinkMarkup)
	assert.Equal(t, 28, link.LinkMarkupStart)
	assert.Equal(t, 67, link.LinkMarkupEnd)

	assert.Equal(t, "", link.DisplayText)
	assert.Equal(t, -1, link.DisplayTextStart)
	assert.Equal(t, -1, link.DisplayTextEnd)

	assert.Equal(t, "https://hackmd.io/_uploads/111.png", link.Source)
	assert.Equal(t, 32, link.SourceStart)
	assert.Equal(t, 66, link.SourceEnd)

	assert.Equal(t, "", link.OptionalPart)
	assert.Equal(t, -1, link.OptionalPartStart)
	assert.Equal(t, -1, link.OptionalPartEnd)

	// [1]
	link = links[1]
	assert.Equal(t, `![](https://hackmd.io/_uploads/222.png "optional part")`, link.LinkMarkup)
	assert.Equal(t, 104, link.LinkMarkupStart)
	assert.Equal(t, 159, link.LinkMarkupEnd)

	assert.Equal(t, "", link.DisplayText)
	assert.Equal(t, -1, link.DisplayTextStart)
	assert.Equal(t, -1, link.DisplayTextEnd)

	assert.Equal(t, "https://hackmd.io/_uploads/222.png", link.Source)
	assert.Equal(t, 108, link.SourceStart)
	assert.Equal(t, 142, link.SourceEnd)

	assert.Equal(t, `"optional part"`, link.OptionalPart)
	assert.Equal(t, 143, link.OptionalPartStart)
	assert.Equal(t, 158, link.OptionalPartEnd)

	// [2]
	link = links[2]
	assert.Equal(t, `![image text](https://hackmd.io/_uploads/333.png "optional part")`, link.LinkMarkup)
	assert.Equal(t, 179, link.LinkMarkupStart)
	assert.Equal(t, 244, link.LinkMarkupEnd)

	assert.Equal(t, "image text", link.DisplayText)
	assert.Equal(t, 181, link.DisplayTextStart)
	assert.Equal(t, 191, link.DisplayTextEnd)

	assert.Equal(t, "https://hackmd.io/_uploads/333.png", link.Source)
	assert.Equal(t, 193, link.SourceStart)
	assert.Equal(t, 227, link.SourceEnd)

	assert.Equal(t, `"optional part"`, link.OptionalPart)
	assert.Equal(t, 228, link.OptionalPartStart)
	assert.Equal(t, 243, link.OptionalPartEnd)
}
func TestLinkMarkup_ParseLinks_MultipleWhiteSpaces(t *testing.T) {
	content := `![](https://hackmd.io/_uploads/1.png    "optional part")`
	links := domain.GetMediaLinks(content)
	assert.NotNil(t, links)
	assert.Len(t, links, 1)

	// [0]
	link := links[0]
	assert.Equal(t, `![](https://hackmd.io/_uploads/1.png    "optional part")`, link.LinkMarkup)
	assert.Equal(t, 0, link.LinkMarkupStart)
	assert.Equal(t, 56, link.LinkMarkupEnd)

	assert.Equal(t, "", link.DisplayText)
	assert.Equal(t, -1, link.DisplayTextStart)
	assert.Equal(t, -1, link.DisplayTextEnd)

	assert.Equal(t, "https://hackmd.io/_uploads/1.png", link.Source)
	assert.Equal(t, 4, link.SourceStart)
	assert.Equal(t, 36, link.SourceEnd)

	assert.Equal(t, `"optional part"`, link.OptionalPart)
	assert.Equal(t, 40, link.OptionalPartStart)
	assert.Equal(t, 55, link.OptionalPartEnd)
}
