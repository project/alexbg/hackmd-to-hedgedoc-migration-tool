package domain

import (
	"fmt"
	"mdmigrate/internal/config"
	"path"
	"regexp"
	"strings"
)

type INote interface {
	GetID() string
	GetTitle() string
	GetContent() string
}

func GetNoteFolderPath(note INote, cfg *config.HackMDConfig) string {
	folder, _ := GetNoteTmpFolderAndFilePath(note, cfg)
	return folder
}
func GetNoteFilePath(note INote, cfg *config.HackMDConfig) string {
	folder, fileName := GetNoteTmpFolderAndFilePath(note, cfg)
	return path.Join(folder, fileName)
}
func GetNoteTmpFolderAndFilePath(note INote, cfg *config.HackMDConfig) (string, string) {
	noteFileName := GetNoteFileName(note, cfg.TopicNameMaxLen)
	tmpNotePath := path.Join(cfg.TmpFolder, noteFileName)
	return tmpNotePath, noteFileName
}
func removeInvalidChars(str string) string {
	return regexp.MustCompile(`[^a-zA-Z0-9а-яА-Я ]+`).ReplaceAllString(str, "")
}
func GetNoteFileName(note INote, topicNameMaxLen int) string {
	title := []rune(note.GetTitle())
	sliceLen := len(title)
	if sliceLen > topicNameMaxLen {
		sliceLen = topicNameMaxLen
	}

	id := removeInvalidChars(note.GetID())
	str := string(title[0:sliceLen])
	str = removeInvalidChars(str)
	tmpFileName := fmt.Sprintf("%s - %s.md", strings.TrimSpace(id), strings.TrimSpace(str))
	return tmpFileName
}
