package domain

import (
	"path/filepath"
	"regexp"
)

type LinkMarkup struct {
	LinkMarkup        string
	LinkMarkupStart   int
	LinkMarkupEnd     int
	Source            string
	SourceStart       int
	SourceEnd         int
	DisplayText       string
	DisplayTextStart  int
	DisplayTextEnd    int
	OptionalPart      string
	OptionalPartStart int
	OptionalPartEnd   int
}

func newLinkMarkup(content string, match []int) *LinkMarkup {
	result := &LinkMarkup{
		LinkMarkupStart:   -1,
		LinkMarkupEnd:     -1,
		SourceStart:       -1,
		SourceEnd:         -1,
		DisplayTextStart:  -1,
		DisplayTextEnd:    -1,
		OptionalPartStart: -1,
		OptionalPartEnd:   -1,
	}

	start := match[0]
	end := match[1]
	if start >= 0 && end > start {
		result.LinkMarkup = content[start:end]
		result.LinkMarkupStart = start
		result.LinkMarkupEnd = end
	}

	start = match[2]
	end = match[3]
	if start >= 0 && end > start {
		result.DisplayText = content[start:end]
		result.DisplayTextStart = start
		result.DisplayTextEnd = end
	}

	start = match[4]
	end = match[5]
	if start >= 0 && end > start {
		result.Source = content[start:end]
		result.SourceStart = start
		result.SourceEnd = end
	}

	start = match[6]
	end = match[7]
	if start >= 0 && end > start {
		result.OptionalPart = content[start:end]
		result.OptionalPartStart = start
		result.OptionalPartEnd = end
	}

	return result
}
func GetMediaLinks(content string) []*LinkMarkup {
	r := regexp.MustCompile(`!\[(?P<text>.*?)\]\((?P<url>.*?)\s*(?P<optionalpart>\".*\")?\)`)
	matches := r.FindAllStringSubmatchIndex(content, -1)
	result := make([]*LinkMarkup, len(matches))
	for i := 0; i < len(matches); i++ {
		linkMarkup := newLinkMarkup(content, matches[i])
		result[i] = linkMarkup
	}
	return result
}
func (link *LinkMarkup) GetSourceFileName() string {
	return filepath.Base(link.Source)
}
