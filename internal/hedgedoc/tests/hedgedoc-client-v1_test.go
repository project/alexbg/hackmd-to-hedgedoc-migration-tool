package hedgedoc

import (
	"mdmigrate/internal/config"
	"mdmigrate/internal/hedgedoc"
	"mdmigrate/pkg/logger"
	"mdmigrate/pkg/osp"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetCurrentUser(t *testing.T) {
	cfg := config.Config{Logger: &logger.DefaultLogger{}}
	err := cfg.Load("../../../.secrets/config-test.yaml", &osp.DefaultOsProvider{})
	assert.NoError(t, err)

	c := hedgedoc.ClientV1(&cfg.HedgeDoc, cfg.Logger)
	user, err := c.GetCurrentUser()
	assert.NoError(t, err)
	assert.Equal(t, "ok", user.GetStatus())
	assert.Equal(t, "4b7655be-78d1-4b5c-843c-78f5b116cd58", user.GetID())
}
