package hedgedoc

import (
	"io/fs"
	"mdmigrate/internal/config"
	"mdmigrate/internal/domain"
	"mdmigrate/internal/hedgedoc"
	"mdmigrate/pkg/osp"
	"net/http"
	"testing"

	domaintest "mdmigrate/internal/domain/tests"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

var testUploaderConfig = &config.Config{
	HackMD:   config.HackMDConfig{TmpFolder: `./hackmd`, TopicNameMaxLen: 10},
	HedgeDoc: config.HedgeDocConfig{UserName: "test-user", UploadsUrl: `http://destination/uploads`},
}

func TestUploader_InvalidUser(t *testing.T) {
	user := &MockUser{}
	user.On("GetName").Return("another_user")

	client := &MockHedgedocClient{}
	client.On("GetCurrentUser").Return(user, nil)

	u := hedgedoc.CreateUploader(testUploaderConfig, client, &osp.MockOsProvider{})
	err := u.Run()

	assert.Error(t, err, hedgedoc.ErrorInvalidUser.Error())
	user.AssertExpectations(t)
	client.AssertExpectations(t)
}
func TestUploader_CheckCurrentUser_Error(t *testing.T) {
	client := &MockHedgedocClient{}
	client.On("GetCurrentUser").Return(nil, http.ErrNotSupported)

	u := hedgedoc.CreateUploader(testUploaderConfig, client, &osp.MockOsProvider{})
	err := u.Run()

	assert.Error(t, err, http.ErrNotSupported.Error())
	client.AssertExpectations(t)
}
func TestUploader_CheckCurrentUser_Success(t *testing.T) {
	user := &MockUser{}
	user.On("GetName").Return(testUploaderConfig.HedgeDoc.UserName)

	client := &MockHedgedocClient{}
	client.On("GetCurrentUser").Return(user, nil)

	osp := &osp.MockOsProvider{}
	osp.On("ReadDir", mock.Anything).Return([]fs.DirEntry{}, nil)

	u := hedgedoc.CreateUploader(testUploaderConfig, client, osp)
	err := u.Run()

	assert.NoError(t, err)
	user.AssertExpectations(t)
	client.AssertExpectations(t)
	osp.AssertExpectations(t)
}
func TestUploader_UploadNote_CantFindMDFile(t *testing.T) {
	user := &MockUser{}
	user.On("GetName").Return(testUploaderConfig.HedgeDoc.UserName)

	note := domaintest.CreateMockNote("ID1", "Title", "# Test note")

	client := &MockHedgedocClient{}
	client.On("GetCurrentUser").Return(user, nil)

	noteDirEntry := &osp.MockDirEntry{}
	noteDirEntry.On("Name").Return(domain.GetNoteFileName(note, testUploaderConfig.HackMD.TopicNameMaxLen)).
		On("IsDir").Return(true)

	osp := &osp.MockOsProvider{}
	osp.On("ReadDir", testUploaderConfig.HackMD.TmpFolder).Return([]fs.DirEntry{noteDirEntry}, nil).
		On("ReadDir", domain.GetNoteFolderPath(note, &testUploaderConfig.HackMD)).Return([]fs.DirEntry{}, nil)

	u := hedgedoc.CreateUploader(testUploaderConfig, client, osp)
	err := u.Run()

	assert.Error(t, err, hedgedoc.ErrorMDFileNotFound)
	user.AssertExpectations(t)
	client.AssertExpectations(t)
	osp.AssertExpectations(t)
}
func TestUploader_UploadNote_NoImages_Success(t *testing.T) {
	user := &MockUser{}
	user.On("GetName").Return(testUploaderConfig.HedgeDoc.UserName)

	note := domaintest.CreateMockNote("ID1", "Title", "# Test note")

	client := &MockHedgedocClient{}
	client.On("GetCurrentUser").Return(user, nil).
		On("CreateNote", mock.Anything).Return(nil)

	noteDirEntry := &osp.MockDirEntry{}
	noteDirEntry.On("Name").Return(domain.GetNoteFileName(note, testUploaderConfig.HackMD.TopicNameMaxLen)).
		On("IsDir").Return(true)

	noteFileEntry := &osp.MockDirEntry{}
	noteFileEntry.On("Name").Return(domain.GetNoteFileName(note, testUploaderConfig.HackMD.TopicNameMaxLen)).
		On("IsDir").Return(false)

	osp := &osp.MockOsProvider{}
	osp.On("ReadDir", testUploaderConfig.HackMD.TmpFolder).Return([]fs.DirEntry{noteDirEntry}, nil).
		On("ReadDir", domain.GetNoteFolderPath(note, &testUploaderConfig.HackMD)).Return([]fs.DirEntry{noteFileEntry}, nil).
		On("ReadFile", domain.GetNoteFilePath(note, &testUploaderConfig.HackMD)).Return(note.GetContent(), nil)

	u := hedgedoc.CreateUploader(testUploaderConfig, client, osp)
	err := u.Run()

	assert.NoError(t, err)
	user.AssertExpectations(t)
	client.AssertExpectations(t)
	osp.AssertExpectations(t)
}
