package hedgedoc

import (
	"io/fs"
	"mdmigrate/internal/config"
	"mdmigrate/internal/domain"
	"mdmigrate/internal/hedgedoc"
	"mdmigrate/pkg/osp"
	"path"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

var defaultTestConfig = &config.Config{
	HackMD:   config.HackMDConfig{TmpFolder: `./hackmd`, TopicNameMaxLen: 10},
	HedgeDoc: config.HedgeDocConfig{UploadsUrl: `http://destination/uploads`},
}

func TestConverter_NoImages(t *testing.T) {
	note := CreateHedgedocMockNote("ID1", "TestNote",
		`# Test Note`,
	)

	u := hedgedoc.CreateConvertor(note, defaultTestConfig, &osp.MockOsProvider{})
	convertedNote, err := u.ConvertLinks()

	assert.NoError(t, err)
	assert.Equal(t, note.GetContent(), convertedNote.GetContent())
	assert.Equal(t, note.GetID(), convertedNote.GetID())
	assert.Equal(t, note.GetTitle(), convertedNote.GetTitle())
}
func TestConverter_ImageDownloaded(t *testing.T) {
	note := CreateHedgedocMockNote("ID1", "TestNote",
		`# Test Note
		![](https://hackmd.io/_uploads/1.png)`,
	)

	osp := &osp.MockOsProvider{}
	osp.On("Stat", domain.GetNoteFolderPath(note, &defaultTestConfig.HackMD)+"/1.png").Return(nil, nil)

	u := hedgedoc.CreateConvertor(note, defaultTestConfig, osp)
	convertedNote, err := u.ConvertLinks()
	expected := strings.ReplaceAll(note.GetContent(),
		"https://hackmd.io/_uploads",
		path.Join(defaultTestConfig.HedgeDoc.UploadsUrl, note.GetID()),
	)

	assert.NoError(t, err)
	osp.AssertExpectations(t)
	assert.Equal(t, expected, convertedNote.GetContent())
	assert.Equal(t, note.GetID(), convertedNote.GetID())
	assert.Equal(t, note.GetTitle(), convertedNote.GetTitle())
}
func TestConverter_ImageIsNotDownloaded(t *testing.T) {
	note := CreateHedgedocMockNote("ID1", "TestNote",
		`# Test Note
		![](https://hackmd.io/_uploads/1.png)`,
	)

	osp := &osp.MockOsProvider{}
	osp.On("Stat", domain.GetNoteFolderPath(note, &defaultTestConfig.HackMD)+"/1.png").Return(nil, fs.ErrNotExist)

	u := hedgedoc.CreateConvertor(note, defaultTestConfig, osp)
	convertedNote, err := u.ConvertLinks()

	assert.NoError(t, err)
	osp.AssertExpectations(t)
	assert.Equal(t, note.GetContent(), convertedNote.GetContent())
	assert.Equal(t, note.GetID(), convertedNote.GetID())
	assert.Equal(t, note.GetTitle(), convertedNote.GetTitle())
}
func TestConverter_TwoImages_SecondIsntDownloaded(t *testing.T) {
	note := CreateHedgedocMockNote("ID1", "TestNote",
		`# Test Note
		![First Image](https://hackmd.io/_uploads/1.png)
		Second Image ![Isn't downloaded](https://hackmd.io/_uploads/2.png)
		`,
	)

	osp := &osp.MockOsProvider{}
	osp.On("Stat", domain.GetNoteFolderPath(note, &defaultTestConfig.HackMD)+"/1.png").Return(nil, nil)
	osp.On("Stat", domain.GetNoteFolderPath(note, &defaultTestConfig.HackMD)+"/2.png").Return(nil, fs.ErrNotExist)

	u := hedgedoc.CreateConvertor(note, defaultTestConfig, osp)
	convertedNote, err := u.ConvertLinks()
	expected := strings.ReplaceAll(note.GetContent(),
		"https://hackmd.io/_uploads/1.png",
		path.Join(defaultTestConfig.HedgeDoc.UploadsUrl, note.GetID(), "1.png"),
	)

	assert.NoError(t, err)
	osp.AssertExpectations(t)
	assert.Equal(t, expected, convertedNote.GetContent())
	assert.Equal(t, note.GetID(), convertedNote.GetID())
	assert.Equal(t, note.GetTitle(), convertedNote.GetTitle())
}
func TestConverter_TwoImages_DifferentLengthOfURL_1(t *testing.T) {
	note := CreateHedgedocMockNote("ID1", "TestNote",
		`# Test Note
		![First Image](https://0123456789/1.png)
		Second Image ![some text](https://01234/2.png)
		`,
	)
	cfg := *defaultTestConfig
	cfg.HedgeDoc.UploadsUrl = "https://0123456/"

	osp := &osp.MockOsProvider{}
	osp.On("Stat", domain.GetNoteFolderPath(note, &cfg.HackMD)+"/1.png").Return(nil, nil)
	osp.On("Stat", domain.GetNoteFolderPath(note, &cfg.HackMD)+"/2.png").Return(nil, nil)

	u := hedgedoc.CreateConvertor(note, &cfg, osp)
	convertedNote, err := u.ConvertLinks()
	expected := strings.ReplaceAll(note.GetContent(),
		"https://0123456789/1.png",
		path.Join(cfg.HedgeDoc.UploadsUrl, note.GetID(), "1.png"),
	)
	expected = strings.ReplaceAll(expected,
		"https://01234/2.png",
		path.Join(cfg.HedgeDoc.UploadsUrl, note.GetID(), "2.png"),
	)

	assert.NoError(t, err)
	osp.AssertExpectations(t)
	assert.Equal(t, expected, convertedNote.GetContent())
	assert.Equal(t, note.GetID(), convertedNote.GetID())
	assert.Equal(t, note.GetTitle(), convertedNote.GetTitle())
}
func TestConverter_TwoImages_DifferentLengthOfURL_2(t *testing.T) {
	note := CreateHedgedocMockNote("ID1", "TestNote",
		`# Test Note
		![Second Image](https://01234/2.png)
		First Image ![some text](https://0123456789/1.png)
		`,
	)
	cfg := *defaultTestConfig
	cfg.HedgeDoc.UploadsUrl = "https://0123456/"

	osp := &osp.MockOsProvider{}
	osp.On("Stat", domain.GetNoteFolderPath(note, &cfg.HackMD)+"/1.png").Return(nil, nil)
	osp.On("Stat", domain.GetNoteFolderPath(note, &cfg.HackMD)+"/2.png").Return(nil, nil)

	u := hedgedoc.CreateConvertor(note, &cfg, osp)
	convertedNote, err := u.ConvertLinks()
	expected := strings.ReplaceAll(note.GetContent(),
		"https://0123456789/1.png",
		path.Join(cfg.HedgeDoc.UploadsUrl, note.GetID(), "1.png"),
	)
	expected = strings.ReplaceAll(expected,
		"https://01234/2.png",
		path.Join(cfg.HedgeDoc.UploadsUrl, note.GetID(), "2.png"),
	)

	assert.NoError(t, err)
	osp.AssertExpectations(t)
	assert.Equal(t, expected, convertedNote.GetContent())
	assert.Equal(t, note.GetID(), convertedNote.GetID())
	assert.Equal(t, note.GetTitle(), convertedNote.GetTitle())
}
func TestConverter_TwoImages_DifferentLengthOfURL_3(t *testing.T) {
	note := CreateHedgedocMockNote("ID1", "TestNote",
		`# Test Note
		![Second Image](https://01234/2.png)
		First Image ![some text](https://0123456789/1.png)
		`,
	)
	cfg := *defaultTestConfig
	cfg.HedgeDoc.UploadsUrl = "https://0123456789ABCEDF/"

	osp := &osp.MockOsProvider{}
	osp.On("Stat", domain.GetNoteFolderPath(note, &cfg.HackMD)+"/1.png").Return(nil, nil)
	osp.On("Stat", domain.GetNoteFolderPath(note, &cfg.HackMD)+"/2.png").Return(nil, nil)

	u := hedgedoc.CreateConvertor(note, &cfg, osp)
	convertedNote, err := u.ConvertLinks()
	expected := strings.ReplaceAll(note.GetContent(),
		"https://0123456789/1.png",
		path.Join(cfg.HedgeDoc.UploadsUrl, note.GetID(), "1.png"),
	)
	expected = strings.ReplaceAll(expected,
		"https://01234/2.png",
		path.Join(cfg.HedgeDoc.UploadsUrl, note.GetID(), "2.png"),
	)

	assert.NoError(t, err)
	osp.AssertExpectations(t)
	assert.Equal(t, expected, convertedNote.GetContent())
	assert.Equal(t, note.GetID(), convertedNote.GetID())
	assert.Equal(t, note.GetTitle(), convertedNote.GetTitle())
}
func TestConverter_TwoImages_DifferentLengthOfURL_4(t *testing.T) {
	note := CreateHedgedocMockNote("ID1", "TestNote",
		`# Test Note
		![Second Image](https://01234/2.png)
		First Image ![some text](https://0123456789/1.png)
		`,
	)
	cfg := *defaultTestConfig
	cfg.HedgeDoc.UploadsUrl = "https://01/"

	osp := &osp.MockOsProvider{}
	osp.On("Stat", domain.GetNoteFolderPath(note, &cfg.HackMD)+"/1.png").Return(nil, nil)
	osp.On("Stat", domain.GetNoteFolderPath(note, &cfg.HackMD)+"/2.png").Return(nil, nil)

	u := hedgedoc.CreateConvertor(note, &cfg, osp)
	convertedNote, err := u.ConvertLinks()
	expected := strings.ReplaceAll(note.GetContent(),
		"https://0123456789/1.png",
		path.Join(cfg.HedgeDoc.UploadsUrl, note.GetID(), "1.png"),
	)
	expected = strings.ReplaceAll(expected,
		"https://01234/2.png",
		path.Join(cfg.HedgeDoc.UploadsUrl, note.GetID(), "2.png"),
	)

	assert.NoError(t, err)
	osp.AssertExpectations(t)
	assert.Equal(t, expected, convertedNote.GetContent())
	assert.Equal(t, note.GetID(), convertedNote.GetID())
	assert.Equal(t, note.GetTitle(), convertedNote.GetTitle())
}
func TestConverter_TwoImages_DifferentLengthOfURL_5(t *testing.T) {
	note := CreateHedgedocMockNote("ID1", "TestNote",
		`# Test Note
		![Second Image](https://0123456789ABCDEF/2.png)
		First Image ![some text](https://0123456789ABCDEF/1.png)
		`,
	)
	cfg := *defaultTestConfig
	cfg.HedgeDoc.UploadsUrl = "https://01/"

	osp := &osp.MockOsProvider{}
	osp.On("Stat", domain.GetNoteFolderPath(note, &cfg.HackMD)+"/1.png").Return(nil, nil)
	osp.On("Stat", domain.GetNoteFolderPath(note, &cfg.HackMD)+"/2.png").Return(nil, nil)

	u := hedgedoc.CreateConvertor(note, &cfg, osp)
	convertedNote, err := u.ConvertLinks()
	expected := strings.ReplaceAll(note.GetContent(),
		"https://0123456789ABCDEF/1.png",
		path.Join(cfg.HedgeDoc.UploadsUrl, note.GetID(), "1.png"),
	)
	expected = strings.ReplaceAll(expected,
		"https://0123456789ABCDEF/2.png",
		path.Join(cfg.HedgeDoc.UploadsUrl, note.GetID(), "2.png"),
	)

	assert.NoError(t, err)
	osp.AssertExpectations(t)
	assert.Equal(t, expected, convertedNote.GetContent())
	assert.Equal(t, note.GetID(), convertedNote.GetID())
	assert.Equal(t, note.GetTitle(), convertedNote.GetTitle())
}
