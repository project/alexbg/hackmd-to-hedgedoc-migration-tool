package hedgedoc

import (
	"mdmigrate/internal/domain"
	"mdmigrate/internal/hedgedoc"

	"github.com/stretchr/testify/mock"
)

type MockHedgedocClient struct {
	mock.Mock
}

func (m *MockHedgedocClient) GetCurrentUser() (hedgedoc.IUser, error) {
	args := m.Called()
	v := args.Get(0)
	if v == nil {
		return nil, args.Error(1)
	}
	return v.(hedgedoc.IUser), args.Error(1)
}
func (m *MockHedgedocClient) CreateNote(note domain.INote) error {
	args := m.Called(note)
	return args.Error(0)
}

type MockUser struct {
	mock.Mock
}

func (m *MockUser) GetStatus() string {
	args := m.Called()
	return args.String(0)
}
func (m *MockUser) GetID() string {
	args := m.Called()
	return args.String(0)
}
func (m *MockUser) GetName() string {
	args := m.Called()
	return args.String(0)
}
