package hedgedoc

import "github.com/stretchr/testify/mock"

type HedgedocMockNote struct {
	mock.Mock
}

func (m *HedgedocMockNote) GetID() string {
	args := m.Called()
	return args.String(0)
}
func (m *HedgedocMockNote) GetTitle() string {
	args := m.Called()
	return args.String(0)
}
func (m *HedgedocMockNote) GetContent() string {
	args := m.Called()
	return args.String(0)
}
func (m *HedgedocMockNote) GetMediaFiles() []string {
	args := m.Called()
	return args.Get(0).([]string)
}

func CreateHedgedocMockNote(id, title, content string) *HedgedocMockNote {
	note := &HedgedocMockNote{}
	note.On("GetID").Return(id).
		On("GetTitle").Return(title).
		On("GetContent").Return(content)
	return note
}
