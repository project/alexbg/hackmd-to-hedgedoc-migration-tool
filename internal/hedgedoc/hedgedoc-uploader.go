package hedgedoc

import (
	"io/fs"
	"mdmigrate/internal/config"
	"mdmigrate/internal/domain"
	"mdmigrate/pkg/osp"
	"path"
)

type IHedgedocUploader interface {
	Run() error
}

type hedgedocUploader struct {
	cfg    *config.Config
	client IHedgedocClient
	osp    osp.IOsProvider
}

func CreateUploader(cfg *config.Config, client IHedgedocClient, osp osp.IOsProvider) IHedgedocUploader {
	return &hedgedocUploader{
		cfg:    cfg,
		client: client,
		osp:    osp,
	}
}

func (u *hedgedocUploader) Run() error {
	if err := u.checkCurrentUser(); err != nil {
		return err
	}

	entries, err := u.osp.ReadDir(u.cfg.HackMD.TmpFolder)
	if err != nil {
		return err
	}

	for _, item := range entries {
		if !item.IsDir() {
			continue
		}

		if err := u.uploadNote(path.Join(u.cfg.HackMD.TmpFolder, item.Name())); err != nil {
			return err
		}
	}
	return nil
}

func (u *hedgedocUploader) checkCurrentUser() error {
	user, err := u.client.GetCurrentUser()
	if err != nil {
		return err
	}

	if user.GetName() != u.cfg.HedgeDoc.UserName {
		return ErrorInvalidUser
	}

	return nil
}
func (u *hedgedocUploader) uploadNote(noteFolderPath string) error {
	note, err := CreateNote(noteFolderPath, u.osp)
	if err != nil {
		return err
	}

	converter := CreateConvertor(note, u.cfg, u.osp)
	note, err = converter.ConvertLinks()

	if err := u.uploadMedia(note); err != nil {
		return err
	}

	if err := u.client.CreateNote(note); err != nil {
		return err
	}

	return nil
}
func (u *hedgedocUploader) uploadMedia(note IHedgedocNote) error {
	noteUploadsPath := path.Join(u.cfg.HedgeDoc.UploadsPath, note.GetID())

	if _, err := u.osp.Stat(noteUploadsPath); err != nil {
		if err != fs.ErrNotExist {
			return err
		}

		if err = u.osp.Mkdir(noteUploadsPath, fs.ModePerm); err != nil {
			return err
		}
	}

	mediaFiles := note.GetMediaFiles()
	for _, mediaFileName := range mediaFiles {
		mediaUpdloadFilePath := path.Join(noteUploadsPath, mediaFileName)
		mediaDownloadFilePath := path.Join(domain.GetNoteFolderPath(note, &u.cfg.HackMD), mediaFileName)
		data, err := u.osp.ReadFile(mediaDownloadFilePath)
		if err != nil {
			return err
		}

		if err = u.osp.WriteFile(mediaUpdloadFilePath, data, fs.ModePerm); err != nil {
			return err
		}
	}
	return nil
}
