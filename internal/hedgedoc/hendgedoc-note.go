package hedgedoc

import (
	"io/fs"
	"mdmigrate/internal/domain"
	"mdmigrate/pkg/osp"
	"path"
	"path/filepath"
	"regexp"
	"strings"
)

type IHedgedocNote interface {
	domain.INote
	GetMediaFiles() []string
}

type note struct {
	content    string
	id         string
	title      string
	mediaFiles []string
}

func (n *note) GetID() string {
	return n.id
}
func (n *note) GetTitle() string {
	return n.title
}
func (n *note) GetContent() string {
	return n.content
}
func (n *note) GetMediaFiles() []string {
	return n.mediaFiles
}

func CreateNote(noteFolderPath string, osp osp.IOsProvider) (IHedgedocNote, error) {
	entries, err := osp.ReadDir(noteFolderPath)
	if err != nil {
		return nil, err
	}

	mdFileName, err := findMDFileName(entries)
	if err != nil {
		return nil, err
	}
	mediaFiles := getMediaFiles(entries)

	filePath := path.Join(noteFolderPath, mdFileName)
	fileData, err := osp.ReadFile(filePath)
	if err != nil {
		return nil, err
	}

	id, title := getIdAndTitle(mdFileName)
	result := &note{
		content:    string(fileData),
		title:      title,
		id:         id,
		mediaFiles: mediaFiles,
	}
	return result, nil
}
func getIdAndTitle(mdFileName string) (string, string) {
	r := regexp.MustCompile(`(?P<id>.*?)\s-\s(?P<title>.*?)\.md`)
	matches := r.FindAllStringSubmatch(mdFileName, -1)
	return matches[0][0], matches[0][1]
}
func findMDFileName(entries []fs.DirEntry) (string, error) {
	for _, item := range entries {
		if item.IsDir() {
			continue
		}
		name := item.Name()
		ext := filepath.Ext(name)
		if strings.ToLower(ext) == ".md" {
			return name, nil
		}
	}
	return "", ErrorMDFileNotFound
}
func getMediaFiles(entries []fs.DirEntry) []string {
	result := []string{}
	for _, item := range entries {
		if item.IsDir() {
			continue
		}
		name := item.Name()
		ext := filepath.Ext(name)
		if strings.ToLower(ext) == ".md" {
			continue
		}
		result = append(result, name)
	}
	return result
}
