package hedgedoc

import (
	"io/fs"
	"mdmigrate/internal/config"
	"mdmigrate/internal/domain"
	"mdmigrate/pkg/osp"
	"path"
	"sort"
)

type IContentConvertor interface {
	ConvertLinks() (IHedgedocNote, error)
}

type contentConvertor struct {
	note              IHedgedocNote
	contentTempFolder string
	newUploadsBaseUrl string
	osp               osp.IOsProvider
}

func CreateConvertor(note IHedgedocNote, cfg *config.Config, osp osp.IOsProvider) IContentConvertor {
	return &contentConvertor{
		note:              note,
		contentTempFolder: domain.GetNoteFolderPath(note, &cfg.HackMD),
		newUploadsBaseUrl: cfg.HedgeDoc.UploadsUrl,
		osp:               osp,
	}
}

func (c *contentConvertor) ConvertLinks() (IHedgedocNote, error) {
	content := c.note.GetContent()
	links := domain.GetMediaLinks(content)
	sort.Slice(links, func(x int, y int) bool {
		return links[x].LinkMarkupStart > links[y].LinkMarkupStart
	})

	for _, link := range links {
		yes, fileName, err := c.isSourceDownloaded(link)
		if err != nil {
			return nil, err
		}

		if !yes {
			continue
		}

		content = c.updateSource(content, fileName, link)
	}

	result := &note{
		content:    content,
		id:         c.note.GetID(),
		title:      c.note.GetTitle(),
		mediaFiles: c.note.GetMediaFiles(),
	}
	return result, nil
}
func (c *contentConvertor) isSourceDownloaded(link *domain.LinkMarkup) (bool, string, error) {
	fileName := link.GetSourceFileName()
	filepath := path.Join(c.contentTempFolder, fileName)

	if _, err := c.osp.Stat(filepath); err != nil {
		if err == fs.ErrNotExist {
			return false, "", nil
		}
		return false, "", err
	}

	return true, fileName, nil
}
func (c *contentConvertor) updateSource(content string, fileName string, link *domain.LinkMarkup) string {
	if link.SourceStart < 0 || link.SourceEnd < 0 || link.SourceStart >= link.SourceEnd {
		return content
	}

	leftPart := content[:link.SourceStart]
	updatedPart := c.getUpdatedUploadURL(fileName)
	rightPart := content[link.SourceEnd:]
	return leftPart + updatedPart + rightPart
}
func (c *contentConvertor) getUpdatedUploadURL(fileName string) string {
	return path.Join(c.newUploadsBaseUrl, c.note.GetID(), fileName)
}
