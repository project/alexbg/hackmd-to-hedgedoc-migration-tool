package hedgedoc

import (
	"encoding/json"
	"fmt"
	"mdmigrate/internal/config"
	"mdmigrate/internal/domain"
	"mdmigrate/pkg/logger"
	"net/http"
	"net/url"
)

type hedgedocClientV1 struct {
	cfg *config.HedgeDocConfig
	log logger.ILogger
}

func ClientV1(cfg *config.HedgeDocConfig, log logger.ILogger) IHedgedocClient {
	return &hedgedocClientV1{
		cfg: cfg,
		log: log,
	}
}
func (c *hedgedocClientV1) withCookie(req *http.Request) {
	req.Header.Add("Cookie", c.cfg.Cookie)
}
func (c *hedgedocClientV1) GetCurrentUser() (IUser, error) {
	c.log.Message(fmt.Sprintf("Request: GetCurrentUser"))
	requestURL, err := url.JoinPath(c.cfg.BaseUrl, "me")
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest(http.MethodGet, requestURL, nil)
	if err != nil {
		return nil, err
	}

	c.withCookie(req)

	response, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	result := &user{}
	err = json.NewDecoder(response.Body).Decode(result)
	if err != nil {
		return nil, err
	}

	return result, nil
}
func (c *hedgedocClientV1) CreateNote(note domain.INote) error {
	c.log.Message(fmt.Sprintf("Request: GetCurrentUser"))
	requestURL, err := url.JoinPath(c.cfg.BaseUrl, "me")
	if err != nil {
		return err
	}

	req, err := http.NewRequest(http.MethodGet, requestURL, nil)
	if err != nil {
		return err
	}

	c.withCookie(req)

	if _, err := http.DefaultClient.Do(req); err != nil {
		return err
	}

	return nil
}

type user struct {
	Status string `json:"status"`
	ID     string `json:"id"`
	Name   string `json:"name"`
}

func (u *user) GetStatus() string {
	return u.Status
}
func (u *user) GetID() string {
	return u.ID
}
func (u *user) GetName() string {
	return u.Name
}
