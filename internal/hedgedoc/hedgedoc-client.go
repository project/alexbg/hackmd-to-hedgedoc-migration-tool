package hedgedoc

import "mdmigrate/internal/domain"

type IHedgedocClient interface {
	GetCurrentUser() (IUser, error)
	CreateNote(note domain.INote) error
}

type IUser interface {
	GetStatus() string
	GetID() string
	GetName() string
}
