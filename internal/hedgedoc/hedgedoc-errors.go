package hedgedoc

import "errors"

var (
	ErrorInvalidUser    = errors.New("Current user has Invalid Name")
	ErrorMDFileNotFound = errors.New("Can't find MD file")
)
