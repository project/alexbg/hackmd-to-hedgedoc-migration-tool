package flags

import (
	"flag"
	"time"
)

type MockFlagsProvider struct {
	vars   map[string]func()
	values map[string]interface{}
}

func NewMockFlagsProvider() *MockFlagsProvider {
	return &MockFlagsProvider{
		vars:   make(map[string]func()),
		values: make(map[string]interface{}),
	}
}

func (fp *MockFlagsProvider) SetValue(name string, value interface{}) *MockFlagsProvider {
	fp.values[name] = value
	return fp
}

func (fp *MockFlagsProvider) StringVar(p *string, name string, value string, usage string) {
	fp.vars[name] = func() {
		if fp.values[name] != nil {
			*p = fp.values[name].(string)
		}
	}
}

func (fp *MockFlagsProvider) IntVar(p *int, name string, value int, usage string) {
	fp.vars[name] = func() {
		if fp.values[name] != nil {
			*p = fp.values[name].(int)
		}
	}
}

func (fp *MockFlagsProvider) DurationVar(p *time.Duration, name string, value time.Duration, usage string) {
	fp.vars[name] = func() {
		if fp.values[name] != nil {
			*p = fp.values[name].(time.Duration)
		}
	}
}

func (fp *MockFlagsProvider) DateVar(p *time.Time, name string, value time.Time, usage string) {
	fp.vars[name] = func() {
		if fp.values[name] != nil {
			*p = fp.values[name].(time.Time)
		}
	}
}

func (fp *MockFlagsProvider) SetUsage(usage func()) {
	//flag.Usage = usage
}

func (fp *MockFlagsProvider) GetCommandLine() *flag.FlagSet {
	return &flag.FlagSet{}
}

func (fp *MockFlagsProvider) PrintDefaults() {
	//flag.PrintDefaults()
}

func (fp *MockFlagsProvider) Parse() {
	for key := range fp.vars {
		fp.vars[key]()
	}
}
