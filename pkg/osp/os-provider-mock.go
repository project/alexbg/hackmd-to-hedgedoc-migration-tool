package osp

import (
	"io/fs"
	"os"

	"github.com/stretchr/testify/mock"
)

type MockOsProvider struct {
	mock.Mock
}

func (osp *MockOsProvider) GetEnv(varName string) string {
	args := osp.Called(varName)
	return args.String(0)
}

func (osp *MockOsProvider) Stat(filePath string) (fs.FileInfo, error) {
	args := osp.Called(filePath)
	fileInfo := args.Get(0)
	if fileInfo == nil {
		return nil, args.Error(1)
	}

	return args.Get(0).(fs.FileInfo), args.Error(1)
}

func (osp *MockOsProvider) ReadDir(dirPath string) ([]fs.DirEntry, error) {
	args := osp.Called(dirPath)
	fileInfo := args.Get(0)
	if fileInfo == nil {
		return nil, args.Error(1)
	}

	return args.Get(0).([]fs.DirEntry), args.Error(1)
}

func (osp *MockOsProvider) ReadFile(filePath string) ([]byte, error) {
	args := osp.Called(filePath)
	str := args.Get(0)
	if str == nil {
		return nil, args.Error(1)
	}

	return []byte(args.String(0)), args.Error(1)
}

func (osp *MockOsProvider) Mkdir(path string, perm fs.FileMode) error {
	args := osp.Called(path, perm)
	return args.Error(0)
}
func (osp *MockOsProvider) Create(path string) (*os.File, error) {
	args := osp.Called(path)
	return args.Get(0).(*os.File), args.Error(1)
}
func (osp *MockOsProvider) WriteFile(path string, data []byte, perm fs.FileMode) error {
	args := osp.Called(path, data, perm)
	return args.Error(0)
}
func (osp *MockOsProvider) GetArgs() []string {
	args := osp.Called()
	result := make([]string, len(args))
	for i := range args {
		result[i] = args.String(i)
	}
	return result
}
