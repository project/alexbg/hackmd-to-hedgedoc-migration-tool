package osp

import (
	"io/fs"
	"os"
)

type IOsProvider interface {
	GetEnv(varName string) string
	Stat(filePath string) (fs.FileInfo, error)
	ReadFile(filePath string) ([]byte, error)
	Mkdir(path string, perm fs.FileMode) error
	Create(path string) (*os.File, error)
	WriteFile(name string, data []byte, perm fs.FileMode) error
	GetArgs() []string
	ReadDir(dirPath string) ([]fs.DirEntry, error)
}

type DefaultOsProvider struct {
}

func (osp *DefaultOsProvider) GetEnv(varName string) string {
	return os.Getenv(varName)
}

func (osp *DefaultOsProvider) Stat(filePath string) (fs.FileInfo, error) {
	return os.Stat(filePath)
}

func (osp *DefaultOsProvider) ReadFile(filePath string) ([]byte, error) {
	return os.ReadFile(filePath)
}

func (osp *DefaultOsProvider) Mkdir(path string, perm fs.FileMode) error {
	return os.Mkdir(path, perm)
}

func (osp *DefaultOsProvider) Create(path string) (*os.File, error) {
	return os.Create(path)
}

func (osp *DefaultOsProvider) WriteFile(path string, data []byte, perm fs.FileMode) error {
	return os.WriteFile(path, data, perm)
}

func (osp *DefaultOsProvider) ReadDir(dirPath string) ([]fs.DirEntry, error) {
	return os.ReadDir(dirPath)
}

func (osp *DefaultOsProvider) GetArgs() []string {
	return os.Args
}
