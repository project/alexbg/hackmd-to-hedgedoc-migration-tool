package osp

import (
	"io/fs"

	"github.com/stretchr/testify/mock"
)

type MockDirEntry struct {
	mock.Mock
}

func (m *MockDirEntry) Name() string {
	args := m.Called()
	return args.String(0)
}
func (m *MockDirEntry) IsDir() bool {
	args := m.Called()
	return args.Bool(0)
}
func (m *MockDirEntry) Type() fs.FileMode {
	args := m.Called()
	return args.Get(0).(fs.FileMode)
}
func (m *MockDirEntry) Info() (fs.FileInfo, error) {
	args := m.Called()
	return args.Get(0).(fs.FileInfo), args.Error(0)
}
