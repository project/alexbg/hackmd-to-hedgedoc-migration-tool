package logger

import "log"

type ILogger interface {
	Message(message string, v ...any)
	Warning(message string, v ...any)
	Error(message string, v ...any)
	Fatal(message string)
}

type DefaultLogger struct {
}

func (l *DefaultLogger) Message(message string, v ...any) {
	log.Printf(message, v...)
}
func (l *DefaultLogger) Warning(message string, v ...any) {
	log.Printf(message, v...)
}
func (l *DefaultLogger) Error(message string, v ...any) {
	log.Printf(message, v...)
}
func (l *DefaultLogger) Fatal(message string) {
	log.Fatal(message)
}
